﻿using App1.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TestApp1.Models;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пользовательский элемент управления" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234236

namespace TestApp1.Controls
{
    public sealed partial class HistoryControl : UserControl
    {
        public AddEventControl addEvent;
        private Style RedStyle = new Style();
        private Style GreenStyle = new Style();
        public HistoryControl()
        {
            this.InitializeComponent();
            RedStyle.Setters.Add(new Setter { Property = Control.BackgroundProperty, Value = new SolidColorBrush(Colors.HotPink)});
            GreenStyle.Setters.Add(new Setter { Property = Control.BackgroundProperty, Value = new SolidColorBrush(Colors.GreenYellow)});
            UpdateList();
        }
        /// <summary>
        /// Обновление истории событий
        /// </summary>
        public void UpdateList()
        {
            using (EventContext db = new EventContext())
            {
                //сортировка по порядку появления
                var list = db.Events.ToList();
                list.Sort((x, y) => y.EventID.CompareTo(x.EventID));
                HistoryGrid.ItemsSource = list;

                double summ = 0;
                foreach (Event ev in db.Events.ToList())
                    if (ev.EventType == OpType.Доход)
                        summ += ev.EventSum;
                    else
                        summ -= ev.EventSum;
                SummLabel.Text = $"Текущий баланс: {summ}";
                if (summ > 0)
                    SummLabel.Foreground = new SolidColorBrush(Colors.DarkGreen);
                else
                    SummLabel.Foreground = new SolidColorBrush(Colors.Red);

            }
        }
        /// <summary>
        /// Выбор одного из элементов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HistoryGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (addEvent != null && HistoryGrid.SelectedItem!=null)
            {
                Event @event = (Event)HistoryGrid.SelectedItem;
                addEvent.GetEditEvent(@event);
            }
        }
    }
}
