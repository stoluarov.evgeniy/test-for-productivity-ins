﻿using System;
using System.Linq;
using TestApp1;
using TestApp1.Controls;
using TestApp1.Models;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Документацию по шаблону элемента "Пользовательский элемент управления" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234236

namespace App1.Controls
{
    public sealed partial class AddEventControl : UserControl
    {
        public HistoryControl list;
        private Event EditableEvent=null;
        public AddEventControl()
        {
            this.InitializeComponent();
            OpTypeBox.ItemsSource = Enum.GetValues(typeof(OpType)).Cast<OpType>();
        }
        /// <summary>
        /// Выбор типа операции (доход/расход)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpTypeBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (OpCategoryBox != null && OpTypeBox.SelectedValue != null)
            {
                if (OpTypeBox.SelectedValue.ToString() == OpType.Доход.ToString())
                    OpCategoryBox.ItemsSource = Config.OpIncomeCategories;
                else
                    OpCategoryBox.ItemsSource = Config.OpOutcomeCategories;
            }
        }
        /// <summary>
        /// Загрузка элемента для редактирования
        /// </summary>
        /// <param name="event"></param>
        public void GetEditEvent(Event @event)
        {
            EditableEvent = @event;
            Delete.Visibility = Visibility.Visible;
            OpTypeBox.SelectedIndex = (int)@event.EventType;
            OpCategoryBox.SelectedValue = @event.EventCategoty.ToString();
            SumBox.Text = @event.EventSum.ToString();
            CommentBox.Text = @event.Comment;
        }
        /// <summary>
        /// Редактирование/добавление
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (OpCategoryBox.SelectedIndex == -1 || OpTypeBox.SelectedIndex == -1)
                return;
            if (!(double.Parse(SumBox.Text) > 0))
                return;
            using (EventContext db = new EventContext())
            {
                if (EditableEvent != null)
                {
                    EditableEvent.EventType = (OpType)OpTypeBox.SelectedIndex; EditableEvent.EventCategoty = OpCategoryBox.SelectedValue.ToString(); EditableEvent.EventSum = double.Parse(SumBox.Text); EditableEvent.Comment = CommentBox.Text;
                    db.Events.Update(EditableEvent);
                }
                else
                    db.Events.Add(new Event { EventType = (OpType)OpTypeBox.SelectedIndex, EventCategoty = OpCategoryBox.SelectedValue.ToString(), EventSum = double.Parse(SumBox.Text), Comment = CommentBox.Text });
                db.SaveChanges();
            }
            list.UpdateList();
        }
        /// <summary>
        /// Очистка формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            OpTypeBox.SelectedIndex = -1;
            OpCategoryBox.SelectedIndex = -1;
            SumBox.Text = "";
            CommentBox.Text = "";
            EditableEvent = null;
            Delete.Visibility = Visibility.Collapsed;
        }
        /// <summary>
        /// Удалить событие
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            using (EventContext db = new EventContext())
            {
                db.Remove(EditableEvent);
                db.SaveChanges();
                list.UpdateList();
            }
        }
        /// <summary>
        /// Проверка ввода суммы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SumBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string line = SumBox.Text;
            for (int i = 0; i < line.Length;)
                if ((line[i] < '0' || line[i] > '9') && line[i] != '.')
                    line = line.Remove(i, 1);
                else
                    i++;
            SumBox.Text = line;
        }
    }
}
