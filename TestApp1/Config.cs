﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp1.Models;

namespace TestApp1
{
    public enum OpType
    {
        Доход, Расход
    }

    public static class Config
    {
        public static List<string> OpIncomeCategories = new List<string>()
        {
            "Зарплата", "Возврат долга", "Дивиденды"
        };
        public static List<string> OpOutcomeCategories = new List<string>()
        {
            "Еда", "ЖКХ", "Развлечения", "Хобби"
        };

        public static List<Event> Events = new List<Event>();
    }
}
