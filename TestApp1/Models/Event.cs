﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp1.Models
{
    public class Event
    {
        public int EventID { get; set; }
        public OpType EventType { get; set; }
        public string EventCategoty { get; set; }
        public double EventSum { get; set; }
        public string Comment { get; set; }
    }
}
