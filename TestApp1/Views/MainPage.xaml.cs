﻿using System;

using TestApp1.ViewModels;

using Windows.UI.Xaml.Controls;

namespace TestApp1.Views
{
    public sealed partial class MainPage : Page
    {
        public MainViewModel ViewModel { get; } = new MainViewModel();

        public MainPage()
        {
            InitializeComponent();
            ControlPanel.list = HistoryPanel;
            HistoryPanel.addEvent = ControlPanel;
        }
    }
}
